  #include <Wire.h>
  #include <BH1750.h> 

  //bikin objek buat BH1750 nya
  BH1750 lightMeter; 

  void setup() {
  Serial.begin(9600);
  Wire.begin(); // buat mulai komunikasi 12C standar, kalo mau beda pin bisa di masukin Wire.begin(SDA,SCL)
  lightMeter.begin(); // mulai start si BH1750 nya (inisialisasi sensor nya)
  Serial.print("TEST BH1750"); 
    
  }
  
  void loop() {
   float lux = lightMeter.readLightLevel();  // buat nyimpen si nilai LUX (kayaknya satuan cahaya) trus kita print
   Serial.print(lux); // LUX di google itu satuan dari daya pancar cahaya 
    delay(1500);
  } 

  // CATATAN YANG MUNGKIN PENTING YANG SIAPA TAU DI TANYA DOSEN
/*
 * 1 lux sama dengan segini 
1 lx = 1 lm/m2 = 1 cd·sr/m2.  

KITA PAKE MODE CONTINUE JADI SELALU NGUKUR SEKITAR
*/  
